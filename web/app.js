// var express = require('express');
// app = express();
// server = require('http').createServer(app);
// io = require('socket.io').listen(server);
//
// var SerialPort = require("serialport")//.SerialPort
// var serialPort = new SerialPort("/COM4", { baudRate: 9600 });
//
// server.listen(8080);
// app.use(express.static('public'));
//
// var brightness = 0;
//
// io.sockets.on('connection', function (socket) {
//     socket.on('led', function (data) {
//         brightness = data.value;
//
//         var buf = new Buffer(1);
//         buf.writeUInt8(brightness, 0);
//         serialPort.write(buf);
//
//         io.sockets.emit('led', {value: brightness});
//     });
//
//     socket.emit('led', {value: brightness});
// });
//
// console.log("Web Server Started go to 'http://localhost:8080' in your Browser.");
const path = require('path');
const basePath = __dirname;
var http = require('http');
var fs = require('fs');
// var index = fs.readFileSync( 'index.html');


var assets = path.join(basePath,'assets');
// var {SerialPort} = require('serialport');
// const {parsers} = SerialPort.parsers;
//
// const parser = new parsers.Readline({
//     delimiter: '\r\n'
// });
const { SerialPort } = require('serialport')
const { ReadlineParser } = require('@serialport/parser-readline')
const port = new SerialPort({ path: '/dev/ttyACM0', baudRate: 9600 })

const parser = port.pipe(new ReadlineParser({ delimiter: '\r\n' }))
//parser.on('data', console.log)

// var port = new SerialPort('/dev/ttyACM0',{
//     baudRate: 9600,
//     dataBits: 8,
//     parity: 'none',
//     stopBits: 1,
//     flowControl: false
// });

//port.pipe(parser);

var app = http.createServer(function(req, res) {
    if(req.url === "/"){
        fs.readFile("./public/index.html", "UTF-8", function(err, html){
            res.writeHead(200, {"Content-Type": "text/html"});
            res.end(html);
        });
    }else if(req.url.match("\.css$")){
        var cssPath = path.join(__dirname, 'public', req.url);
        var fileStream = fs.createReadStream(cssPath, "UTF-8");
        res.writeHead(200, {"Content-Type": "text/css"});
        fileStream.pipe(res);

    }else if(req.url.match("\.png$")){
        var imagePath = path.join(__dirname, 'public', req.url);
        var fileStream = fs.createReadStream(imagePath);
        res.writeHead(200, {"Content-Type": "image/png"});
        fileStream.pipe(res);
    }else{
        res.writeHead(404, {"Content-Type": "text/html"});
        res.end("No Page Found");
    }
    //res.writeHead(200, {'Content-Type': 'text/html'});
    //res.end(index);
});

var io = require('socket.io')(app);

io.on('connection', function(socket) {

    console.log('Node is listening to port');
    socket.on('led', function (data) {

        brightness = data.value;

        var buf = Buffer.alloc(5);
        let n = 0;
        buf.writeUInt8(brightness, n++);
        buf.writeUInt8(brightness, n++);
        // serialPort.write(buf);
        console.log(buf)

        // io.sockets.emit('led', {value: brightness});
    });
});



parser.on('data', function(data) {

    //console.log('Received data from port: ' + data);
    io.emit('data', data);

});

app.listen(3000);